package math

import (
	"log"
	"math"
)

// GetPercentileByValue returns a percentile by a given on a given data set.
func GetPercentileByValue(value int, data []int) int {
	dl := float64(len(data))

	if 0 == dl {
		return 100
	}

	var lt float64

	for _, v := range data {
		if v <= value {
			lt++
		}
	}

	log.Println(lt, dl, lt / dl)

	return int(math.Floor(lt / dl * 100))
}
