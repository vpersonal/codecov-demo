package math

import "testing"

func TestGetPercentileByValue(t *testing.T) {
	type test struct {
		data []int
		value int
		expectedPercentile int
	}

	tests := []test{
		{
			data: []int{},
			value: 0,
			expectedPercentile: 100,
		},
		{
			data: []int{1},
			value: 0,
			expectedPercentile: 0,
		},
		{
			data: []int{1},
			value: 1,
			expectedPercentile: 100,
		},
		{
			data: []int{1},
			value: 2,
			expectedPercentile: 100,
		},
		{
			data: []int{1, 3, 5, 2, 88, 2, 4, 5},
			value: 13,
			expectedPercentile: 87,
		},
	}

	for _, v := range tests {
		r := GetPercentileByValue(v.value, v.data)
		if r != v.expectedPercentile {
			t.Errorf("Expected %v, got %v", v.expectedPercentile, r)
		}
	}
}
