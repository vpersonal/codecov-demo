package main

import (
	"fmt"
	"gitlab.com/vpersonal/codecov-demo/math"
)

func main() {
	dh := []int{7, 1, 7, 2, 7, 7, 7, 2, 9, 3, 4, 5, 5, 6}
	dhv := 6

	r := math.GetPercentileByValue(dhv, dh)
	fmt.Printf("%d is %dth percentile for a given data set\n", dhv, r)
}
